<?php
/**
 * The header for our theme
 *
 */
 
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="site">

	<header id="site-header">

    <!--<?php get_template_part("/templates/template-parts/header/utility-bar"); ?>-->

    <div class="header-main">
      <div class="d-block d-md-flex justify-content-md-between align-items-md-center flex-wrap container">
        <div class="site-branding py-2 py-md-1">
          <div class="logo">
            <?php echo site_logo(); ?>
          </div>
          <div class="mobile-toggle d-md-none"><i class="fa fa-bars" aria-hidden="true"></i></div>
        </div><!-- .site-branding -->

        <div class="d-flex align-items-end justify-content-end align-items-end">
          <?php get_template_part("/templates/template-parts/header/main-nav"); ?>

          <div class="text-center d-none d-lp-block">
            <span class="h5 text-contact "><a href="/contact-us">GET A FREE ESTIMATE</a></span><br><a class=" mb-0 text-white font-weight-bold h4 btn btn-contact" href="tel:<?php echo do_shortcode('[lg-phone-main]'); ?>"><?php echo do_shortcode('[lg-phone-main]'); ?></a>
          </div>
        </div>
      </div>
    </div>

  </header><!-- #masthead -->

  <?php if(!is_blog() && !is_404()): ?>
      <?php get_template_part("/templates/template-parts/page/feature-slider"); ?>
    <?php endif; ?>