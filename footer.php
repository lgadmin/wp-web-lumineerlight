<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
	<div class="container-wide pb-4 footer-map">
		<?php echo do_shortcode('[lg-map id=155]') ?>
	</div>
	<footer id="site-footer">
		
		<div id="site-footer-main" class="clearfix py-4 container-fluid justify-content-center align-items-start flex-wrap">
			<div class="container">
				<div class="row">
					<div class="site-footer-alpha col-md-6 col-lg-2 text-center text-lg-left"><?php dynamic_sidebar('footer-alpha'); ?></div>
					<div class="site-footer-bravo d-none d-lg-block col-lg-8 text-center text-lg-left"><?php dynamic_sidebar('footer-bravo'); ?></div>
					<div class="site-footer-charlie col-md-6 col-lg-2 text-center text-lg-left mt-3 mt-lg-0"><?php dynamic_sidebar('footer-charlie'); ?></div>
				</div>
			</div>
		</div>

		<div id="site-legal" class="pb-3 px-3">
			<div class="container d-flex justify-content-center justify-content-md-between align-items-center flex-wrap">
				<div class="site-info"><?php get_template_part("/templates/template-parts/footer/site-info"); ?></div>
				<div class="site-longevity"> <?php get_template_part("/templates/template-parts/footer/site-footer-longevity"); ?> </div>
			</div>
		</div>

	</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>
