<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/components/acf-flexible-layout/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

	<?php
		$max_width = get_sub_field('max_width');
	?>

	<div class="col-12" style="<?php if($max_width): ?><?php echo 'margin: auto; max-width: '.$max_width; ?><?php endif; ?>">
		<?php the_sub_field('editor'); ?>
	</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/components/acf-flexible-layout/partials/block-settings-end');

?>
