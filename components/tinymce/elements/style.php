<?php

	global $lg_tinymce_style;

	$lg_tinymce_style = array(
	    'title' => 'Style',
	    'items' =>  array(
			array(
				'title' => 'Border Box',
				'block' => 'div',
				'classes'	=> 'border-box',
				'wrapper'	=> true
			)
	    )
	)

?>