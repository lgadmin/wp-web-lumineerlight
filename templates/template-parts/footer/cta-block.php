<div class="py-4 bg-secondary">
	<div class="container cta-block d-flex flex-wrap justify-content-start justify-content-md-between text-white align-items-center">
		<?php if($cta_title): ?>
			<div class="h4 my-2 text-white"><?php echo $cta_title; ?></div>
		<?php endif; ?>

		<?php if($cta_button): ?>
			<a href="<?php echo $cta_button['url']; ?>" class="btn btn-primary h5 mb-0 mt-2 mt-md-0"><?php echo $cta_button['title']; ?></a>
		<?php endif; ?>
	</div>
</div>