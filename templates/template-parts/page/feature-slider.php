<?php

	$feature_slider_active = get_field('feature_slider_active');
	$form_active = get_field('form_active');
	$page_default_banner = get_field('page_default_banner', 'option');
	$product_default_banner = get_field('product_default_banner', 'option');
	$blog_default_banner = get_field('blog_default_banner', 'option');
?>

<?php

if( have_rows('feature_slider') && $feature_slider_active == 1 ):
	?>
		<div class="feature-slider-wrap">
			<div class="feature-slider">
				<?php
				    while ( have_rows('feature_slider') ) : the_row();
				        $image = get_sub_field('image');
				        ?>
				        <div><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"></div>
				        <?php
				    endwhile;
			    ?>
		    </div>

		    <?php if($form_active): ?>
			    <div class="overlay_wrap py-4 d-none d-sm-block">
				    <div class="overlay container">
					    <?php if($form_active): ?>
					    	<?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true tabindex=49]'); ?>
					    <?php endif; ?>
				    </div>
			    </div>
			<?php endif; ?>

	    </div>
    <?php
else :
    ?>
    <div class="feature-image-default">
		<?php
			switch ($post->post_type) {
				case 'page':
					?>
						<img class="img-fit" src="<?php echo $page_default_banner['url']; ?>" alt="<?php echo $page_default_banner['alt']; ?>">
					<?php
					break;
				case 'post':
					?>
						<img class="img-fit" src="<?php echo $product_default_banner['url']; ?>" alt="<?php echo $product_default_banner['alt']; ?>">
					<?php
					break;
				case 'product':
					?>
						<img class="img-fit" src="<?php echo $blog_default_banner['url']; ?>" alt="<?php echo $blog_default_banner['alt']; ?>">
					<?php
					break;

				default: 
					?>
						<img class="img-fit" src="<?php echo $page_default_banner['url']; ?>" alt="<?php echo $page_default_banner['alt']; ?>">
					<?php
					break;
			}
		?>

		 <?php if($form_active): ?>
			    <div class="overlay_wrap py-4 d-none d-sm-block">
				    <div class="overlay container">
					    <?php if($form_active): ?>
					    	<?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true tabindex=49]'); ?>
					    <?php endif; ?>
				    </div>
			    </div>
		<?php endif; ?>
	</div>
    <?php
endif;

?>