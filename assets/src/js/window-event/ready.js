// Windows Ready Handler

(function($) {

    $(document).ready(function(){

      	//Mobile Toggle
        $('.mobile-toggle').on('click', function(){
      		$('.navbar-collapse').fadeToggle();
      	});

       if($(window).width() > 768){

          $('#site-header').find('#main-navbar >ul >li').on('mouseover', function(){
            $(this).find('.dropdown-menu').addClass('show');
          });

          $('#site-header').find('#main-navbar >ul >li').on('mouseleave', function(){
            $(this).find('.dropdown-menu').removeClass('show');
          });
        }else{
          $('#site-header').find('#main-navbar >ul >li >a').on('click', function(){
            $(this).siblings('.dropdown-menu').toggleClass('show');
          });
        }

        $('.feature-slider').slick({
          autoplay: true,
          fade: true,
          arrows: false,
          dots: false,
          autoplaySpeed: 6000,
          adaptiveHeight: true
        });

        lightbox.option({
          'resizeDuration': 700,
          'wrapAround': true,
          'disableScrolling': true,
          'positionFromTop': 0
        })
        
    });

}(jQuery));